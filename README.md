# go-common

[![Go Reference](https://pkg.go.dev/badge/gitlab.com/act3-ai/asce/go-common.svg)](https://pkg.go.dev/gitlab.com/act3-ai/asce/go-common)

This project houses Go code shared between ASCE projects. It mostly focuses on logging, CLI, and REST API functionality.

---

Approved for public release: distribution unlimited. Case Number: AFRL-2024-1060
